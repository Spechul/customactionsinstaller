﻿using System;
using System.Globalization;
using System.IO;
using System.Timers;

namespace BackgroundJobService.Services
{
    public class HeartbeatService
    {
        private Timer _timer;
        private readonly string _path;

        public HeartbeatService(string path)
        {
            _path = path;
            _timer = new Timer(1000) { AutoReset = true };
            _timer.Elapsed += OnTimerElapsed;
        }

        private void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            var lines = new string[] {DateTime.UtcNow.ToString(CultureInfo.InvariantCulture)};
            File.AppendAllLines(_path, lines);
        }

        public void Start()
        {
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
        }
    }
}
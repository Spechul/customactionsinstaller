﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BackgroundJobService.Services;
using Topshelf;

namespace BackgroundJobService
{
    class Program
    {
        static void Main(string[] args)
        {
            var exitCode = HostFactory.Run(x =>
            {
                x.Service<HeartbeatService>(s =>
                {
                    s.ConstructUsing(heartbeat => new HeartbeatService("log.txt"));
                    s.WhenStarted(heartbeat => heartbeat.Start());
                    s.WhenStopped(heartbeat => heartbeat.Stop());
                });

                x.RunAsLocalSystem();
                x.SetServiceName("HeartbeatService");
                x.SetDisplayName("Heartbeat Service");
                x.SetDescription("spams datetime each second to a file");
                x.EnableServiceRecovery(srv =>
                {
                    srv.OnCrashOnly();
                    srv.RestartService(new TimeSpan(0, 0, 0, 10)); // 1-st failure
                    srv.RestartService(new TimeSpan(0, 0, 0, 10)); // 2-nd failure
                    srv.RestartService(new TimeSpan(0, 0, 0, 10)); // subsequent failures
                    srv.SetResetPeriod(1);
                });
            });

            int intExitCode = Convert.ToInt32(exitCode);
            Environment.ExitCode = intExitCode;
        }
    }
}
